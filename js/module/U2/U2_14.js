export function clsup_e() {
    if (localStorage.getItem("item").indexOf("U2cat_maid") > -1 || localStorage.getItem("item").indexOf("U2cat_out") > -1) {//猫娘套装可以喝牛奶
        clsshow(
            "想喝什么吗？",
            "<p>在你挣扎进卧室之后，人群稍微没那么拥挤了。小酒吧旁边站着一个男人，正盯着你看。</p><p>“嘿，想喝点什么吗小可爱？我给你这样的女孩准备了一些特别的东西，不过我是不会免费送你的。”，那个男人往你这边走了走，“那么，你想要什么呢？”</p><br><p>-就来杯啤酒吧，谢谢<br>-喵呜～（牛奶）<br>-苦艾酒（特殊的小礼品）</p>",
            0,
            "U2/U2_14",
            //'<a href="#!" class="modal-close waves-effect waves-green btn-flat" onclick="clsfin(\'U2/U2_14\',\'bear\')">啤酒</a><a href = "#!" class= "modal-close waves-effect waves-green btn-flat" onclick = "clsfin(\'U2/U2_14\',\'milk\')" > 喵呜～</a><a href="#!" class="modal-close waves-effect waves-green btn-flat" onclick="clsfin(\'U2/U2_14\',\'ai\')">苦艾酒</a>'
            [{ "btnfun": "clsfin(\'U2/U2_14\',\'bear\')", "btnname": "啤酒" }, { "btnfun": "clsfin(\'U2/U2_14\',\'ai\')", "btnname": "苦艾酒" }, { "btnfun": "clsfin(\'U2/U2_14\',\'milk\')", "btnname": "喵呜～" }]
        );
    }
    else {
        clsshow(
            "想喝什么吗？",
            "<p>在你挣扎进卧室之后，人群稍微没那么拥挤了。小酒吧旁边站着一个男人，正盯着你看。</p><p>“嘿，想喝点什么吗小可爱？我给你这样的女孩准备了一些特别的东西，不过我是不会免费送你的。”，那个男人往你这边走了走，“那么，你想要什么呢？”</p><br><p>-就来杯啤酒吧，谢谢<br>-苦艾酒（特殊的小礼品）</p>",
            0,
            "U2/U2_14",
            //'<a href="#!" class="modal-close waves-effect waves-green btn-flat" onclick="clsfin(\'U2/U2_14\',\'bear\')">啤酒</a><a href = "#!" class= "modal-close waves-effect waves-green btn-flat" onclick = "clsfin(\'U2/U2_14\',\'ai\')" > 苦艾酒</a > '
            [{ "btnfun": "clsfin(\'U2/U2_14\',\'bear\')", "btnname": "啤酒" }, { "btnfun": "clsfin(\'U2/U2_14\',\'ai\')", "btnname": "苦艾酒" }]
        );
    }
}
export function clsfin_e(info) {
    switch (info) {
        case "bear": {
            clsshow(
                "想喝什么吗？",
                "<p>喝500ml啤酒</p>",
                0,
                "U2/U2_14",
                //'<a href="#!" class="modal-close waves-effect waves-green btn-flat disabled timeset" onclick="clsfin(\'U2/U2_14\',\'done\')">完成</a>'
                [{ "btnfun": "clsfin(\'U2/U2_14\',\'done\')", "btnname": "完成" }]
            )
            break;
        }
        case "milk": {
            clsshow(
                "想喝什么吗？",
                "<p>喝一杯温牛奶</p>",
                0,
                "U2/U2_14",
                //'<a href="#!" class="modal-close waves-effect waves-green btn-flat disabled timeset" onclick="clsfin(\'U2/U2_14\',\'done\')">完成</a>'
                [{ "btnfun": "clsfin(\'U2/U2_14\',\'done\')", "btnname": "完成" }]
            )
            break;
        }
        case "ai": {
            var timeset = 5, word = "", sizer = 3, sizel = 14, bpm = 60;//处理buff
            if (localStorage.getItem("buff-oral")) {
                let json = JSON.parse(localStorage.getItem("buff-oral"));
                timeset += parseInt(json.timeset);
                word = json.word;
                sizer += parseInt(json.sizer);
                sizel += parseInt(json.sizel);
                bpm += parseInt(json.bpm);
                sissy(parseInt(json.sissy));
                slut(parseInt(json.slut));
                alpha(parseInt(json.alpha))
                money(parseInt(json.moneyin));
            }
            clsshow(
                "想喝什么吗？",
                "<p>口交" + timeset + "分钟，BPM为" + bpm + "，干一小杯苦艾酒</p>+<p>" + word + ";使用直径为" + sizer + "长度为" + sizel + "的阳具</p>",
                timeset * 60,
                "U2/U2_14",
                //'<a href="#!" class="modal-close waves-effect waves-green btn-flat disabled timeset" onclick="clsfin(\'U2/U2_14\',\'done\')">完成</a>'
                [{ "btnfun": "clsfin(\'U2/U2_14\',\'done\')", "btnname": "完成" }]
            )
            break;
        }
        case "done": {
            if (localStorage.getItem("buff-U2")) {
                if (localStorage.getItem("buff-U2").indexOf("U2_14") == -1) {
                    localStorage.setItem("buff-U2", localStorage.getItem("buff-U2") + "/meow/U2_14");
                }
            } else {
                localStorage.setItem("buff-U2", "U2_14");
            }
            clsshow(
                "想喝什么吗？",
                "<p>请选择下一步进入哪个关卡</p><p>部分关卡具有可重复性，不过，你的进度将回到该关卡。</p><p>你可以在帮助信息中找到关卡解释图</p>",
                0,
                "U2/U2_14",
                //' <a href="#!" class="modal-close waves-effect waves-green btn-flat" onclick="clsfin(\'U2/U2_14\',\'0\')">前往“餐厅”关卡</a><a href="#!" class="modal-close waves-effect waves-green btn-flat" onclick="clsfin(\'U2/U2_14\',\'1\')">前往“洗劫冰箱”关卡</a>'
                [{ "btnfun": "clsfin(\'U2/U2_14\',\'0\')", "btnname": "前往“餐厅”关卡" }, { "btnfun": "clsfin(\'U2/U2_14\',\'1\')", "btnname": "前往“洗劫冰箱”关卡" }]
            );
            break;
        }
        case "0": {//餐厅
            localStorage.setItem("cls", "U2_15");
            /*dialogAlert("本关卡完成", "成功");*/
            ClsPicOff();
            app.initia();
            break;
        }
        case "1": {//洗劫冰箱
            localStorage.setItem("cls", "U2_16");
            /*dialogAlert("本关卡完成", "成功");*/
            ClsPicOff();
            app.initia();
            break;
        }
        default: {
            dialogAlert("系统错误，请点击课程卡片获取课程编码并向群里报告此问题");
            break;
        }
    }
}


/*  <a href="#!" class="modal-close waves-effect waves-green btn-flat" onclick="clsfin(\'U0/U0_0\',\'sissy\')">你正在喝果汁（伪娘值+1）</a>
  <a href="#!" class="modal-close waves-effect waves-green btn-flat" onclick="clsfin(\'U0/U0_0\',\'slut\')">你正在喝啤酒（母狗值+1）</a>
  <a href="#!" class="modal-close waves-effect waves-green btn-flat" onclick="clsfin(\'U0/U0_0\',\'alpha\')">你正在喝威士忌（Alpha值+1）</a>*/
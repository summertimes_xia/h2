"use strict"
function uni(code) { //主进度记录
    if (typeof (nomodule) != "undefined") {
        alert("您的浏览器可能不支持模块加载技术，请升级浏览器。推荐电脑使用Firefox，手机使用MS edge")
    }
    buffload();
    localStorage.setItem("uni", code);
    document.getElementById("loading").style.display = "block"
    app.initia();
    if (code > 3) {
        money(1000);
    }
}

function clsrestart() { //课程继续
    try {
        var clsup = JSON.parse(localStorage.getItem("clsup"));
    } catch (err) {
        dialogAlert("哦呜，课程数据损坏！课白上了");
        localStorage.removeItem("clsup");
        location.reload();
    }
    let title = clsup["title"];
    let footer = clsup["footer"];
    let requires = clsup["require"];
    let node = clsup["node"]

    //开始弹出图片
    ClsPicOn(node, localStorage.getItem("setting-pic"))
    //向拟态框填充内容
    document.getElementById("botm_header").innerHTML = title;
    document.getElementById("botm_content").innerHTML = requires + "<p id=\"CountDown\">距离结束还有好多秒</p>";
    document.getElementById("botm_footer").innerHTML = footer;
    localStorage.setItem("timeset", clsup["endtime"]);
    botm.open();
    ctd = window.setInterval(ctdgo, 1000); //启动倒计时
}



//更改分数
function alpha(d) {
    if (localStorage.getItem("alpha")) {
        localStorage.setItem("alpha", parseFloat(localStorage.getItem("alpha")) + parseFloat(d));
    } else {
        localStorage.setItem("alpha", d);
    }
}

function sissy(d) {
    if (localStorage.getItem("sissy")) {
        localStorage.setItem("sissy", parseFloat(localStorage.getItem("sissy")) + parseFloat(d));
    } else {
        localStorage.setItem("sissy", d);
    }
}

function slut(d) {
    if (localStorage.getItem("slut")) {
        localStorage.setItem("slut", parseFloat(localStorage.getItem("slut")) + parseFloat(d));
    } else {
        localStorage.setItem("slut", d);
    }
}

function heart(d) {
    if (localStorage.getItem("heart")) {
        localStorage.setItem("heart", parseFloat(localStorage.getItem("heart")) + parseFloat(d));
    } else {
        localStorage.setItem("heart", d);
    }
    if (parseFloat(localStorage.getItem("heart")) > 3 && !localStorage.getItem("house") && parseFloat(localStorage.getItem("uni")) > 2) {
        localStorage.setItem("house", true);
        dialogAlert("恭喜你，已经解锁公寓系统")
    }
    if (parseFloat(localStorage.getItem("heart")) < -4) {
        localStorage.setItem("uni", "gameover");
    }
}

function money(d) {
    if (d < 0) { //你不可以欠钱不还
        if (parseFloat(localStorage.getItem("money")) + parseFloat(d) < 0 || !localStorage.getItem("money")) {
            return false;
        }
    }
    if (localStorage.getItem("money")) {
        localStorage.setItem("money", parseFloat(localStorage.getItem("money")) + parseFloat(d));
    } else {
        localStorage.setItem("money", d);
    }
    return true;
}

function itemadd(node) { //添加一个物品-调用动态模块
    if (localStorage.getItem("item")) {
        if (localStorage.getItem("item").indexOf(node) > -1) {
            dialogAlert("你已经拥有此物品");
            return;
        }
    }
    if (typeof (nomodule) != "undefined") {
        alert("您的浏览器可能不支持模块加载技术，请升级浏览器。推荐电脑使用Firefox，手机使用MS edge")
    }

    console.info("本地服务器动态加载buff" + node)
    import(GetUrl()+'js/module/buff/' + node + '.js')
        .then(module => {
            module.add_e(); //由该模块进行处理
        })
        .catch(err => {
            console.error(err);
            dialogAlert(node + "模块加载失败，是bug呢！请在聊天部分调教咕咕喵。错误说明：" + err.message);
        });

}

function itemadd2(code) { //物品添加由模块在这儿改存储
    if (localStorage.getItem("item")) {
        if (localStorage.getItem("item").indexOf(code) > -1) {
            dialogAlert("你已经拥有此物品");
            return;
        }
    }
    if (!localStorage.getItem("item")) {
        localStorage.setItem("item", code);
    } else {
        localStorage.setItem("item", localStorage.getItem("item") + "/meow/" + code);
    }
    dialogAlert("添加成功", "成功");
    app.initia(); //重新刷新
}

function itemremove(code) { //弹出移除物品确认框框
    var callback = new Function("e", 'if(e==2){return}else{itemremove2(\'' + code + '\');}') //
    if (navigator.notification) {
        navigator.notification.confirm("真的要移除这个物品吗？", callback, "确认？？？", ["确定", "取消"]);
    } else {
        if (confirm("真的要移除这个物品吗？")) {
            itemremove2(code);
        } else {
            return;
        }
    }
}

function itemremove2(node) { //开始通知模块进行移除
    node = "buff/" + node;
    if (typeof (nomodule) != "undefined") {
        alert("您的浏览器可能不支持模块加载技术，请升级浏览器。推荐电脑使用Firefox，手机使用MS edge")
    }

    console.info("本地服务器动态加载buff-移除" + node)
    import(GetUrl()+'js/module/' + node + '.js')
        .then(module => {
            module.remove_e(); //由该模块进行处理
            buffload(); //重新刷新buff信息
        })
        .catch(err => {
            console.error(err);
            dialogAlert(node + "模块加载失败，是bug呢！请在聊天部分调教咕咕喵。错误说明：" + err.message);
        });

}

function itemremove3(code) { //由动态模块从存储中移除物品

    if (!localStorage.getItem("item")) {
        dialogAlert("您当前没有任何物品");
        return;
    }

    if (localStorage.getItem("onitem") && localStorage.getItem("onitem").indexOf(code) > -1) { //如果激活了的话先取消激活该组件
        var sp = localStorage.getItem("onitem")
        if (sp == code || sp == "/meow/" + code || sp == code + "/meow/") { //如果只有一个
            localStorage.removeItem("onitem");
        } else if (sp.indexOf("/meow/" + code) > -1) { //如果可以从前部删除
            let end = sp.substr(0, sp.indexOf("/meow/" + code)) + sp.substr(sp.indexOf("/meow/" + code) + code.length + 6, sp.length); //切掉那部分字符串
            localStorage.setItem("onitem", end);
        } else if (sp.indexOf(code + "/meow/") > -1) { //如果可以从前部删除
            let end = sp.substr(0, sp.indexOf(code + "/meow/")) + sp.substr(sp.indexOf(code + "/meow/") + code.length + 6, sp.length); //切掉那部分字符串
            localStorage.setItem("onitem", end);
        } else {
            dialogAlert("移除失败");
            return;
        }
        sp = undefined;
    }
    var sp1 = localStorage.getItem("item")
    if (sp1 == code || sp1 == "/meow/" + code || sp1 == code + "/meow/") { //如果只有一个
        localStorage.removeItem("item");
        dialogAlert("移除成功", "成功")
        return;
    } else if (sp1.indexOf("/meow/" + code) > -1) { //如果可以从前部删除
        var end1 = sp1.substr(0, sp1.indexOf("/meow/" + code)) + sp1.substr(sp1.indexOf("/meow/" + code) + code.length + 6, sp1.length); //切掉那部分字符串
    } else if (sp1.indexOf(code + "/meow/") > -1) { //如果可以从后部删除
        var end1 = sp1.substr(0, sp1.indexOf(code + "/meow/")) + sp1.substr(sp1.indexOf(code + "/meow/") + code.length + 6, sp1.length); //切掉那部分字符串
    }
    if (end1) { //检查是否成功操作字符串，并给出结果
        localStorage.setItem("item", end1)
        dialogAlert("移除成功");
    } else {
        dialogAlert("移除失败");
    }
}

function itemon(code) { //激活某个物品
    if (localStorage.getItem("onitem")) {
        if (localStorage.getItem("onitem").indexOf(code) > -1) {
            dialogAlert("你已激活此物品");
            return;
        }
    }
    if (!localStorage.getItem("onitem")) {
        localStorage.setItem("onitem", code);
    } else {
        localStorage.setItem("onitem", localStorage.getItem("onitem") + "/meow/" + code);
    }
    dialogAlert("激活成功", "成功");
    buffload();

}

function itemoff(code) { //关闭某个物品效果
    if (!localStorage.getItem("onitem")) {
        dialogAlert("您当前没有任何物品被激活");
        return;
    }
    var sp = localStorage.getItem("onitem")
    if (sp == code || sp == "/meow/" + code || sp == code + "/meow/") { //如果只有一个
        localStorage.removeItem("onitem");
        dialogAlert("暂停成功", "成功")
        app.initia(); //重新刷新
        return;
    } else if (sp.indexOf("/meow/" + code) > -1) { //如果可以从前部删除
        var end = sp.substr(0, sp.indexOf("/meow/" + code)) + sp.substr(sp.indexOf("/meow/" + code) + code.length + 6, sp.length); //切掉那部分字符串
    } else if (sp.indexOf(code + "/meow/") > -1) { //如果可以从前部删除
        var end = sp.substr(0, sp.indexOf(code + "/meow/")) + sp.substr(sp.indexOf(code + "/meow/") + code.length + 6, sp.length); //切掉那部分字符串
    }
    if (end) {
        localStorage.setItem("onitem", end);
        dialogAlert("暂停成功", "成功")
    } else {
        dialogAlert("暂停失败")
    }
    app.initia(); //重新刷新

}

function buffload() { //加载buff信息到存储信息

    bufft = 0;
    //移除之前的信息
    localStorage.removeItem("buff-oral");
    localStorage.removeItem("buff-anal");
    localStorage.removeItem("buff-water");
    localStorage.removeItem("buff-enforce");
    localStorage.removeItem("buff-fuck");

    if (!localStorage.getItem("onitem")) {
        console.log("没有任何buff");
        return
    }
    var sp = localStorage.getItem("onitem").split("/meow/");
    var t = 0;
    while (t < sp.length) {
        netload("buff/" + sp[t], sp.length);
        t += 1;
    }

    function netload(node, num) {
        if (typeof (nomodule) != "undefined") {
            alert("您的浏览器可能不支持模块加载技术，请升级浏览器。推荐电脑使用Firefox，手机使用MS edge")
        }
        console.info("本地服务器动态加载buff" + node)
        import(GetUrl()+'js/module/' + node + '.js')
            .then(module => {
                module.buff_e(); //由该模块进行处理
                buffload2(num); //重新刷新
            })
            .catch(err => {
                console.error(err);
                dialogAlert(node + "模块加载失败，是bug呢！请在聊天部分调教咕咕喵。错误说明：" + err.message);
            });

    }
}

function buffload2(num) { //buff异步重新加载
    bufft += 1;
    console.log("buff异步加载" + bufft);
    if (bufft >= num) {
        app.initia(); //执行刷新
        bufft = 0;
    }
}

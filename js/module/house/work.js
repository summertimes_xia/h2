export function clsup_e(){
    let myDate = new Date();
    if(localStorage.getItem("nohouse")){
        if(parseInt(myDate.getTime())<parseInt(localStorage.getItem("nohouse"))){
            dialogAlert("您当前不能使用住所系统，请等待"+String((parseInt(localStorage.getItem("nohouse"))-parseInt(myDate.getTime()))/1000)+"秒");
            return;
        }else{
            localStorage.removeItem("nohouse")
        }
    }
    clsshow(
        '工作室',
        "<p>该干活啦</p><p>请查看下面的投稿说明进行投稿。点击按钮即可增加钱</p><br><p>",
        0,
        'wash',
        [{"btnname":"写一个涩涩的道具集，至少2K字。并将其投稿（$+10)","btnfun":"clsfin('house/work',10)"},{"btnname":"在推特上发一个自己照片（$+20)","btnfun":"clsfin('house/work',20)"},
        {"btnname":"投稿一个短篇的小说，4K字（$+30）","btnfun":"clsfin('house/work',30)"},{"btnname":"向琳投稿一个基于WTCD的动态小说（$+50）","btnfun":"clsfin('house/work',50)"},
        {"btnname":"投稿一个自己画的色图（$+100）","btnfun":"clsfin('house/work',80)"},{"btnname":"为本项目贡献代码或捐赠（$+100）","btnfun":"clsfin('house/work',100)"},
        {"btnname":"投稿详细说明","btnfun":"clsfin('house/work','notice')","noclose":true},{"btnname":"只是随便看看","btnfun":"clsfin('house/work','e')"}]
    )
}

export function clsfin_e(info){
    if(info=='e'){
        return
    }else if(info=='notice'){
        reader('input');   
    }else{
        money(parseInt(info));
    }
}
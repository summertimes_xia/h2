"use strict"
function clsfin(node, info) { //课程弹框关闭
    if(typeof(nomodule)!="undefined"){
        alert("您的浏览器可能不支持模块加载技术，请升级浏览器。推荐电脑使用Firefox，手机使用MS edge")
    }

    try {//尝试检查将调用的模块是否为上课模块
        var clsup = JSON.parse(localStorage.getItem("clsup"));
    } catch (err) {
        console.log("课程数据不存在！跳过一致性检查");
        localStorage.removeItem("clsup");
        location.reload();
    }
    if(node!=localStorage.getItem("module")){
        console.warn("当前调用的模块"+node+"与启动模块"+localStorage.getItem("module")+"不相符，请检查代码");
        dialogAlert("当前调用的模块"+node+"与启动模块"+localStorage.getItem("module")+"不相符，请在群里报告这个错误");
    }
    if(node!=clsup.node){
        console.warn("当前调用的模块"+node+"与图片记录模块"+localStorage.getItem("module")+"不相符，请检查代码");
        dialogAlert("当前调用的模块"+node+"与图片记录模块"+localStorage.getItem("module")+"不相符，请在群里报告这个错误");
    }

    localStorage.removeItem("timeset"); //移除倒计时、上课持续
    localStorage.removeItem("clsup");
    window.clearInterval(ctd);

    //尝试动态加载，若失败提升升级浏览器
        console.info("远程服务器动态加载课程" + node)
        import(GetUrl()+'js/module/' + node + '.js')
            .then(module => {
                module.clsfin_e(info); //由该模块进行处理
            })
            .catch(err => {
                console.error(err);
                if(err instanceof TypeError){//可能是一个bug，但修复啦
                    dialogAlert(node+"模块加载失败，请尝试在设置中清除上课恢复信息；如多次出现，可在聊天中调教咕咕喵！错误说明："+err.message);
                }else{
                    dialogAlert(node+"模块加载失败，是bug呢！请在聊天部分调教咕咕喵。错误说明："+err.message);
                }
            });
}


function clsshow(title, requires, timeset, node, footer) { //进入关卡，json格式为btnfun、btnname分别为方法和按钮名字;如果定义了noclose=true则不用关闭对话框且无视倒计时禁用；如果定义了notime=true则不要管它的限制时间问题
    ctd = window.setInterval(ctdgo, 1000); //启动倒计时
    //向拟态框填充内容
    document.getElementById("botm_header").innerHTML = title;
    document.getElementById("botm_content").innerHTML = requires + "<p id=\"CountDown\">距离结束还有" + timeset + "秒</p>";
    let t=0;
    var footers="";
    if(timeset<=0){
        while(t<footer.length){
            if(footer[t]['noclose']==true || footer['noclose']=="true"){
                footers+='<button href="#!" class="waves-effect waves-green btn-flat" onclick="'+footer[t]["btnfun"]+'">'+footer[t]["btnname"]+'</button>'
            }else{
                footers+='<button href="#!" class="modal-close waves-effect waves-green btn-flat" onclick="'+footer[t]["btnfun"]+'">'+footer[t]["btnname"]+'</button>'
            }
            t+=1;
        }
    }else{
        while(t<footer.length){
            if(footer[t]['noclose']==true || footer['noclose']=="true"){
                footers+='<button href="#!" class="waves-effect waves-green btn-flat timeset" onclick="'+footer[t]["btnfun"]+'">'+footer[t]["btnname"]+'</button>'
            }else if(footer[t]['notime']==true || footer['notime']=="true"){
                footers+='<button href="#!" class="modal-close waves-effect waves-green btn-flat" onclick="'+footer[t]["btnfun"]+'">'+footer[t]["btnname"]+'</button>'
            }else{
                footers+='<button href="#!" class="modal-close waves-effect waves-green btn-flat disabled timeset" onclick="'+footer[t]["btnfun"]+'">'+footer[t]["btnname"]+'</button>'
            }
            t+=1;
        }
    }
    document.getElementById("botm_footer").innerHTML = footers;
    var time = new Date;
    var clsup = {
        node: node,
        endtime: parseInt(time.getTime()) + timeset * 1000,
        title: title,
        require: requires,
        footer: footers
    }
    localStorage.setItem("clsup", JSON.stringify(clsup)); //使课程可恢复
    let d = new Date();
    localStorage.setItem("timeset", parseInt(d.getTime()) + timeset * 1000)
    botm.open();
    ClsPicOn(node, localStorage.getItem("setting-pic"))
}

function clsup(node, info) { //上课
    if (localStorage.getItem("clsup")) { //检查是否有课程继续
        dialogAlert("RBQ需要先上完现在的课程呢！");
        clsrestart()
        return;
    }
    //确保打开的clsup模块相符
    localStorage.setItem("module",node);
    if(typeof(nomodule)!="undefined"){
        alert("您的浏览器可能不支持模块加载技术，请升级浏览器。推荐电脑使用Firefox，手机使用MS edge")
    }
    //尝试动态加载，若失败提升升级浏览器
        console.info("远程服务器动态加载课程" + node)
        import(GetUrl()+'js/module/' + node + '.js')
            .then(module => {
                module.clsup_e(info); //由该模块进行处理
            })
            .catch(err => {
                console.error(err);
                if(err instanceof TypeError){//可能是没做这个关卡
                    dialogAlert(node+"模块加载失败，有可能是这个关卡还没做哦，可以在聊天处调教咕咕喵来催更。错误说明："+err.message);
                }else{
                    dialogAlert(node+"模块加载失败，是bug呢！请在聊天部分调教咕咕喵。错误说明："+err.message);
                }
            });
    
}

export function clsup_e(){
    clsshow(
        "电话",
    '<p>一阵电话铃声将你吵醒了，你看了下表，已经上午10点了。</p><p>打来电话的是她，她的声音听起来有点生疏，就好像她第一次听到你的声音一样。“嘿，嗯…是你吗？我吵醒你了吗？不好意思，听着，你等下来我公寓好吗？太好了！我等着你，待会见！”</p><br><p>精心打扮，就像你马上要去约会了一样(sissy+1)<br>随随便便，穿着你日常衣服去(alpha+1)</p>',
    0,
    "U2/U2_0",
     //' <a href="#!" class="modal-close waves-effect waves-green btn-flat" onclick="clsfin(\'U2/U2_0\',\'0\')">精心打扮</a><a href="#!" class="modal-close waves-effect waves-green btn-flat" onclick="clsfin(\'U2/U2_0\',\'1\')">随随便便</a>'
     [{"btnfun":"clsfin(\'U2/U2_0\',\'0\')","btnname":"精心打扮"},{"btnfun":"clsfin(\'U2/U2_0\',\'1\')","btnname":"随随便便"}]
    );
}
export function clsfin_e(info){
    switch(info){
        case "0":{
            sissy(1);
            break;
        }
        case "1":{
            alpha(1);
            break;
        }
        default:{
            dialogAlert("系统错误，请点击课程卡片获取课程编码并向群里报告此问题")
            break;
        }
    }
    if(localStorage.getItem("couple")=="1"){
        localStorage.setItem("cls","U2_1");
    }else{
        localStorage.setItem("cls","U2_2");
    }
    /*dialogAlert("本关卡完成", "成功");*/
    ClsPicOff();
    app.initia();
}


/* 
<a href="#!" class="modal-close waves-effect waves-green btn-flat" onclick="clsfin(\'U2/U2_0\',\'0\')">精心打扮</a>
<a href="#!" class="modal-close waves-effect waves-green btn-flat" onclick="clsfin(\'U2/U2_0\',\'1\')">随随便便</a>
*/

"use strict"
function dialogAlert(message, title, buttonname, callback) { //通知服务
    title = title || "错误";
    buttonname = buttonname || "确定";
    callback = callback || function () {
        return;
    }
    if (navigator.notification) {
        navigator.notification.alert(message, callback, title, buttonname);
    } else {
        alert(message);
        callback.apply();
    }
}

function loc(ati) { //动画跳转
    document.body.addEventListener("animationend", function () {
        document.location = ati;
    }.bind(this))
    document.body.style.animation = "hidden 0.3s forwards";
}


function ctdgo() { //各类倒计时
    if (!localStorage.getItem("timeset")) {
        console.log("找不到定时点，退出");
        window.clearInterval(ctd);
        return;
    }
    if (localStorage.getItem("setting-notime")) {
        if (localStorage.getItem("setting-notime") == true || localStorage.getItem("setting-notime") == "true") {
            var allcls = document.getElementsByClassName("timeset"); //启用按钮
            var t = 0,
                a = allcls.length;
            while (t <= a && allcls[t] != undefined) {
                allcls[t].classList.remove("disabled");
                t += 1
            }
        }
    }

    let d = new Date(),
        left = Math.ceil((parseInt(localStorage.getItem("timeset")) - parseInt(d.getTime())) / 1000);
        try{
            document.getElementById("CountDown").innerHTML = "距离结束还有" + left + "秒"
        }
        catch(err){
            if(err instanceof TypeError){
                console.log("检测到类型错误："+err+"已清除计时器")
                window.clearInterval(ctd);
            }else{
                console.err(err)
            }
        }
        finally{
            if (left < 1) { //倒计时完成
                window.clearInterval(ctd);
                document.getElementById("CountDown").innerHTML = "";
                var allcls = document.getElementsByClassName("timeset"); //启用按钮
                var t = 0,
                    a = allcls.length;
                while (t <= a && allcls[t] != undefined) {
                    allcls[t].classList.remove("disabled");
                    t += 1
                }
            }
        }
}

function clean() { //清除localstorage信息
    localStorage.clear();
    location.reload();
}

function ctab(tab,title){//动画更换页面
    if(title!==null && title!==undefined && title!=""){
        if(document.getElementById("NavTitle").innerHTML==""){
            document.getElementById("top-nav").style.display="block";
            document.getElementById("NavTitle").innerHTML=title;
            document.getElementById("NavTitle").style.animation = "fadein 0.3s forwards";
            setTimeout(function (){
                document.getElementById("NavTitle").style.animation = "";
            }, 301);
        }else{
            document.getElementById("NavTitle").style.animation = "fadeaway 0.3s forwards";
            setTimeout(function (){
                document.getElementById("NavTitle").innerHTML=title;
                document.getElementById("NavTitle").style.animation = "fadein 0.3s forwards";
            }, 300);
            setTimeout(function (){
                document.getElementById("NavTitle").style.animation = "";
            }, 601);
        }
    }else{
        document.getElementById("NavTitle").style.animation = "fadeaway 0.3s forwards";
        setTimeout(function (){
            document.getElementById("NavTitle").style.animation = "";
            document.getElementById("top-nav").style.display="none";
            document.getElementById("NavTitle").innerHTML="";
        }, 301);
    }


    document.getElementById("tab1").style.animation = "fadeaway 0.3s forwards";
    document.getElementById("tab2").style.animation = "fadeaway 0.3s forwards";
    document.getElementById("tab3").style.animation = "fadeaway 0.3s forwards";
    document.getElementById("tab4").style.animation = "fadeaway 0.3s forwards";
    document.getElementById("tab5").style.animation = "fadeaway 0.3s forwards";

    setTimeout(function (){
        document.getElementById("tab1").style.display="none";
        document.getElementById("tab2").style.display="none";
        document.getElementById("tab3").style.display="none";
        document.getElementById("tab4").style.display="none";
        document.getElementById("tab5").style.display="none";

        document.getElementById(tab).style.display="block";
        document.getElementById(tab).style.animation = "fadein 0.3s forwards";
    }, 300);
    setTimeout(function (){
        document.getElementById("tab1").style.animation = "";
        document.getElementById("tab2").style.animation = "";
        document.getElementById("tab3").style.animation = "";
        document.getElementById("tab4").style.animation = "";
        document.getElementById("tab5").style.animation = "";
    }, 601);
}

function random() { //骰子
    let point = Math.ceil(Math.random() * 6);
    dialogAlert("你的点数为" + point, "骰子");
}

function modal(node, jscode) { //拟态框页面
    var xhr = new XMLHttpRequest();
    xhr.open("GET", './modals/' + node + '.json?'+(new Date()).valueOf(), true);
    xhr.send();
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
            if (xhr.status == 200) {
                let ans = JSON.parse(xhr.responseText);
                document.getElementById("info_header").innerHTML = ans.header;
                document.getElementById("info_content").innerHTML = ans.content;
                document.getElementById("info_footer").innerHTML = ans.footer;
                info.open();
                if (jscode !== null && jscode !== undefined) { //如果有异步操作
                    jscode.apply();
                }
            } else {
                dialogAlert("网络错误，代码" + xhr.status);
            }
        }
    }
} 


function meowout() { //弹出导出数据拟态框
    document.getElementById("info_header").innerHTML = "数据导出";
    document.getElementById("info_content").innerHTML = ' <textarea>' +app.dataout() + '</textarea>';
    document.getElementById("info_footer").innerHTML = '<a href="#!" class="modal-close waves-effect waves-green btn-flat" >关闭</a>';
    setTimeout(function (){
        info.open(); 
    }, 10);
}

function meowin() { //数据导入
    document.location = "meowin.html?meowin=" + document.getElementById("meowin").value;
}

function noval(cha) { //设定小说阅读到章节
    localStorage.setItem("noval", cha);
    app.initia()
}

function SettingSet(name, value) { //设置保存
    if (value === undefined || value === null) {
        localStorage.removeItem(name);
    } else {
        localStorage.setItem(name, value);
    }
}

function SettingCKBLoad(name, id) { //开关设置内容加载
    if (localStorage.getItem(name) == true || localStorage.getItem(name) == "true") {
        document.getElementById(id).checked = "1"
    } else {
        document.getElementById(id).checked = ""
    }
}

function GetUrl(){ //返回当前系统运行的根目录
    let url=window.document.location.href;
    let urll=url.split('/');
    var ans='';
    for (let index = 0; index < urll.length-1; index++) {
        ans += urll[index];
        ans += '/';
    }
    return ans;
}